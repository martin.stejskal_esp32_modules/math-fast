/**
 * @file
 * @author Martin Stejskal
 * @brief Accurate enough but fast mathematics operations
 *
 * Greatly optimized library is at
 * https://www.atwillys.de/content/cc/sine-lookup-for-embedded-in-c/?lang=en
 *
 * However it was quite hard to decode for me, so I create my own look-up
 * table in more "engineering" form.
 */
#ifndef __MATH_FAST_H__
#define __MATH_FAST_H__
// ===============================| Includes |================================
#include <stdbool.h>
#include <stdint.h>
// ================================| Defines |================================
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
// ===========================| Global variables |============================
// ===============================| Functions |===============================
// =========================| High level functions |==========================
/**
 * @brief Return X,Y coordinates based on initial position, angle and length
 *
 * This can be used in functions where is needed to know X,Y position from
 * initial point while angle and length is known.
 *
 * @param i32_init_x Initial X position
 * @param i32_init_y Initial Y position
 * @param i32_length Hypotenuse absolute length
 * @param i32_angle_deg Angle in degrees. Reference is X axe
 * @param pi32_x Calculated X position will be written here
 * @param pi32_y Calculated Y position will be written here
 */
void mf_get_xy_coordinates(const int32_t i32_init_x, const int32_t i32_init_y,
                           const int32_t i32_length, int32_t i32_angle_deg,
                           int32_t *pi32_x, int32_t *pi32_y);
// ========================| Middle level functions |=========================
/**
 * @brief Get angle in degrees based on X Y from coordinates
 *
 * Example:
 *  x = 80 ( -------- )
 *  y = 10 ( |        )
 *  result angle: 7 degrees
 *
 * @param i32_x Absolute X value
 * @param i32_y Absolute Y value
 * @return Angle in degrees. Range 0~360 degrees
 */
int32_t mf_get_angle_from_xy(int32_t i32_x, int32_t i32_y);

/**
 * @brief Check if given angle fits into given boundaries
 * @param i32_angle_deg Signed angle in degrees. Range -2^31 ~ +2^31
 * @param i32_min Minimum angle in degrees. Range -2^31 ~ +2^31
 * @param i32_max Maximum angle in degrees. Range -2^31 ~ +2^31
 * @return True if reference angle is in the range
 */
bool mf_is_angle_in_range(int32_t i32_angle_deg, int32_t i32_min,
                          int32_t i32_max);

/**
 * @brief Calculate relative difference between reference and compare angle
 *
 * @note Work within 0~360 degrees range. So 420 degrees will be treated as
 *       60 degrees. If 420 will be reference angle and compare angle will be
 *       10, result will be -50 degrees.
 *
 * @param i32_angle_reference_deg Reference angle in degrees
 * @param i32_angle_compare_deg Compare angle in degrees
 * @return Relative difference as signed value within -180 ~ +180 degrees range
 */
int32_t mf_relative_angle_difference(int32_t i32_angle_reference_deg,
                                     int32_t i32_angle_compare_deg);
// ==========================| Low level functions |==========================
/**
 * @brief Calculate sin(x) from angle in degrees
 * @param i32_angle_deg Angle in degrees
 * @return sin(x) value multiplied by 1e6
 */
int32_t mf_sin(int32_t i32_angle_deg);
/**
 * @brief Reverse function to sin_fast()
 * @param i32_sin_x The sin(x) value multiplied by 1e6
 * @return Angle from range -90 ~ 90 degrees
 */
int32_t mf_asin(int32_t i32_sin_x);

/**
 * @brief Calculate cos(x) from angle in degrees
 * @param i32_angle_deg Angle in degrees
 * @return cos(x) value multiplied by 1e6
 */
int32_t mf_cos(int32_t i32_angle_deg);

/**
 * @brief Reverse function for cos_fast()
 * @param i32_cos_x The cos(x) value multiplied by 1e6
 * @return Angle from range 0 ~ 180 degrees
 */
int32_t mf_acos(int32_t i32_cos_x);

/**
 * @brief Calculate tan(x) from angle in degrees
 * @param i32_angle_deg Angle in degrees
 * @return tan(x) multiplied by 1e6
 */
int32_t mf_tan(int32_t i32_angle_deg);

/**
 * @brief Reverse function for tan_fast()
 * @param i32_tan_x The tan(x) value multiplied by 1e6
 * @return Angle from range 0 ~ 180 degrees
 */
int32_t mf_atan(int32_t i32_tan_x);

/**
 * @brief Return angle from 0~360 range
 * @param i32_angle_deg Signed angle in degrees
 * @return Angle from 0~360 range
 */
int32_t mf_get_360_deg_range(int32_t i32_angle_deg);

/**
 * @brief Return angle from 0 ~ 180 ; -179 ~ -1 range
 * @param i32_angle_deg Signed angle in degrees
 * @return Angle from 0 ~ 180 ; -179 ~ -1 range
 */
int32_t mf_get_plus_minus_180_deg_range(int32_t i32_angle_deg);

/**
 * @brief Simplified sqrt() for 16 bit input
 *
 * Using "Babylonian method" for calculating square root
 *
 * @param i16_src The 16 bit long input
 * @return The 16 bit long unsigned integer
 */
uint16_t mf_sqrt_16(uint16_t i16_src);

/**
 * @brief Simplified sqrt() for 32 bit input
 *
 * Using "Babylonian method" for calculating square root
 *
 * @param i32_src The 32 bit long input
 * @return The 32 bit long unsigned integer
 */
uint32_t mf_sqrt_32(uint32_t i32_src);

/**
 * @brief Simplified sqrt() for 64 bit input
 *
 * Using "Babylonian method" for calculating square root
 *
 * @param u64_src The 64 bit long input
 * @return The 64 bit long unsigned integer
 */
uint64_t mf_sqrt_64(uint64_t u64_src);

/**
 * @brief Return absolute value for 16 bit value
 * @param i16_src The 16 bit input value
 * @return The 16 bit absolute result
 */
int16_t mf_abs_16(int16_t i16_src);

/**
 * @brief Return absolute value for 32 bit value
 * @param i32_src The 32 bit input value
 * @return The 32 bit absolute result
 */
int32_t mf_abs_32(int32_t i32_src);

/**
 * @brief Return absolute value for 64 bit value
 * @param i64_src The 64 bit input value
 * @return The 64 bit absolute result
 */
int64_t mf_abs_64(int64_t i64_src);

/**
 * @brief Return absolute value for float
 * @param f_src The input float value
 * @return The absolute value
 */
float mf_abs_float(float f_src);
#endif  // __MATH_FAST_H__
