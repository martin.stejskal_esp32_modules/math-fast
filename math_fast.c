/**
 * @file
 * @author Martin Stejskal
 * @brief Accurate enough but fast mathematics operations
 *
 * Greatly optimized library is at
 * https://www.atwillys.de/content/cc/sine-lookup-for-embedded-in-c/?lang=en
 *
 * However it was quite hard to decode for me, so I create my own look-up
 * table in more "engineering" form.
 */
// ===============================| Includes |================================
#include "math_fast.h"

#include <assert.h>
// ================================| Defines |================================

// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
// ===========================| Global variables |============================
/**
 * @brief Look up table for sine
 *
 * Because sine function basically "repeating" itself every 90 degrees (just
 * change phase or polarity), it is OK to just keep just values from range
 * 0~90 degrees.
 * Resolution here is 1 degree per record -> 90 values
 */
const static int32_t mi32_sin90[] = {
    0,       17452,  34899,  52336,  69756,  87156,  104528, 121869, 139173,
    156434,  173648, 190809, 207912, 224951, 241922, 258819, 275637, 292372,
    309017,  325568, 342020, 358368, 374607, 390731, 406737, 422618, 438371,
    453990,  469472, 484810, 500000, 515038, 529919, 544639, 559193, 573576,
    587785,  601815, 615661, 629320, 642788, 656059, 669131, 681998, 694658,
    707107,  719340, 731354, 743145, 754710, 766044, 777146, 788011, 798636,
    809017,  819152, 829038, 838671, 848048, 857167, 866025, 874620, 882948,
    891007,  898794, 906308, 913545, 920505, 927184, 933580, 939693, 945519,
    951057,  956305, 961262, 965926, 970296, 974370, 978148, 981627, 984808,
    987688,  990268, 992546, 994522, 996195, 997564, 998630, 999391, 999848,
    1000000,
};
/**
 * @brief Look up table for tangent
 *
 * This is repeating every 90 degrees. Well, kind of, sometimes it is reversed
 * but idea is same as in case of mi32sin90
 */
const static int32_t mi32_tan90[] = {
    0,         17455,    34921,    52408,    69927,    87489,    105104,
    122785,    140541,   158384,   176327,   194380,   212557,   230868,
    249328,    267949,   286745,   305731,   324920,   344328,   363970,
    383864,    404026,   424475,   445229,   466308,   487733,   509525,
    531709,    554309,   577350,   600861,   624869,   649408,   674509,
    700208,    726543,   753554,   781286,   809784,   839100,   869287,
    900404,    932515,   965689,   1000000,  1035530,  1072369,  1110613,
    1150368,   1191754,  1234897,  1279942,  1327045,  1376382,  1428148,
    1482561,   1539865,  1600335,  1664279,  1732051,  1804048,  1880726,
    1962611,   2050304,  2144507,  2246037,  2355852,  2475087,  2605089,
    2747477,   2904211,  3077684,  3270853,  3487414,  3732051,  4010781,
    4331476,   4704630,  5144554,  5671282,  6313752,  7115370,  8144346,
    9514364,   11430052, 14300666, 19081137, 28636253, 57289962,
    INT32_MAX,  // Infinite, but ...
};
// ===============================| Functions |===============================
// =====================| Internal function prototypes |======================
/**
 * @brief Return angle from 0~180 range
 * @param i32_angle Signed angle in degrees
 * @return Angle from 0~180 range
 */
static int32_t _get_180_deg_range(int32_t i32_angle);
// =========================| High level functions |==========================
void mf_get_xy_coordinates(const int32_t i32_init_x, const int32_t i32_init_y,
                           const int32_t i32_length, int32_t i32_angle,
                           int32_t *pi32_x, int32_t *pi32_y) {
  assert(pi32_x);
  assert(pi32_y);
  // Length should not be negative
  assert(i32_length >= 0);

  // Since sin_fast() returns value multiplied by million, length have to
  // be limited to avoid overflow. 2^31 /1e6 = 2147
  assert(i32_length < 2147);
  // Not sure if initial X/Y should be also checked, since overflow showing
  // on "other side" - maybe it is cool

  *pi32_x = i32_init_x + ((mf_cos(i32_angle) * i32_length) / 1000000);
  *pi32_y = i32_init_y + ((mf_sin(i32_angle) * i32_length) / 1000000);
}
// ========================| Middle level functions |=========================
int32_t mf_get_angle_from_xy(int32_t i32_x, int32_t i32_y) {
  // Basically atan() should do the job. To get tan(x) value is relatively
  // easy: tan(x) = Y/X. But dividing by zero is not cool, so it have to be
  // handled here
  if (i32_x == 0) {
    if (i32_y == 0) {
      // Vector is zero -> logically zero angle
      return 0;
    } else if (i32_y > 0) {
      // Between 1st and 2nd quadrant -> 90 degrees
      return 90;
    } else {
      // Between3rd and 4th quadrant -> 270 degrees
      return 270;
    }
  }

  // That was edge cases. Now serious math. Not forget that atan_fast()
  // works with 1e6 multiplied values. Due to this we need to use 64 bit
  // temporary variable to deal with values properly
  int64_t i64_tmp = (1000000 * (int64_t)i32_y);
  // If X and Y are negative, by dividing we loose sign. But this is not
  // purpose. Purpose is to get accurate result
  i64_tmp /= i32_x;

  // Bit rounding
  if (i64_tmp > INT32_MAX) {
    i64_tmp = INT32_MAX;
  }
  if (i64_tmp < INT32_MIN) {
    i64_tmp = INT32_MIN;
  }

  int32_t i32_angle = mf_atan((int32_t)i64_tmp);

  if ((i32_x >= 0) && (i32_y >= 0)) {
    // 1st quadrant - 0~90. No change
  } else if ((i32_x < 0) && (i32_y >= 0)) {
    // 2nd quadrant - 90~180. There is 1 ambiguous case. Problem is that
    // ratio of Y/X can be basically 0 (due to integer can not be -0.001)
    // and since it is 0, we got angle 0 too -> need to fix that
    if (i32_angle == 0) {
      i32_angle = 180;
    } else {
      // atan() can not take into account quadrants. Need to add correction
      i32_angle = mf_get_360_deg_range(i32_angle - 180);
    }
  } else if ((i32_x < 0) && (i32_y < 0)) {
    // 3rd quadrant - 180~270.
    i32_angle = mf_get_360_deg_range(i32_angle + 180);

  } else if ((i32_x >= 0) && (i32_y < 0)) {
    // 4th quadrant - 270~360. The negative value is given, but in test cases
    // it is expected 0~360 range -> need to recalculate
    i32_angle = mf_get_360_deg_range(i32_angle);
  } else {
    // Should not happen. Maybe universe is collapse?
    assert(0);
    return 0;
  }

  return i32_angle;
}

bool mf_is_angle_in_range(int32_t i32_angle_deg, int32_t i32_min_deg,
                          int32_t i32_max_deg) {
  // Easy check at the beginning. No need to bring heavy machinery there
  if (i32_min_deg == i32_max_deg) {
    if (i32_angle_deg == i32_max_deg) {
      return true;
    } else {
      return false;
    }
  }

  bool b_in_range = false;

  // Get 0~360 range
  i32_angle_deg = mf_get_360_deg_range(i32_angle_deg);
  i32_min_deg = mf_get_360_deg_range(i32_min_deg);
  i32_max_deg = mf_get_360_deg_range(i32_max_deg);

  // Circle is tricky thing, since it "end" at 360 and it is repeated. So
  // simple mathematics can not help us. It is simply need to calculate with
  // transition from 359 to 1 degree (4th -> 1st quadrant)
  if (i32_min_deg < i32_max_deg) {
    // No tricks needed
    if ((i32_angle_deg >= i32_min_deg) && (i32_angle_deg <= i32_max_deg)) {
      b_in_range = true;
    } else {
      b_in_range = false;
    }

  } else {
    // min value > max value. Angle is "reversed" -> simply swap min and max
    // and check again if it is in range. Then negotiate result and we get
    // what we want. It's that simple.
    b_in_range = !(
        mf_is_angle_in_range(i32_angle_deg, i32_max_deg + 1, i32_min_deg - 1));
  }

  return b_in_range;
}

int32_t mf_relative_angle_difference(int32_t i32_angle_reference_deg,
                                     int32_t i32_angle_compare_deg) {
  i32_angle_reference_deg = mf_get_360_deg_range(i32_angle_reference_deg);
  i32_angle_compare_deg = mf_get_360_deg_range(i32_angle_compare_deg);

  int32_t i32_result = i32_angle_compare_deg - i32_angle_reference_deg;

  i32_result = mf_get_plus_minus_180_deg_range(i32_result);

  assert(i32_result <= 180);
  assert(i32_result >= -180);

  return i32_result;
}

// ==========================| Low level functions |==========================
int32_t mf_sin(int32_t i32_angle_deg) {
  i32_angle_deg = mf_get_360_deg_range(i32_angle_deg);

  // Need to find quadrant
  if (i32_angle_deg <= 90) {
    // Direct read
    return mi32_sin90[i32_angle_deg];
  } else if (i32_angle_deg <= 180) {
    // Read "backwards". Deduct offset (0~90 are handled before -> next is
    // 91 degrees). Also very last record (index 90) is actually value for
    // 90 degrees -> need to use one "back" -> 89
    return mi32_sin90[89 - (i32_angle_deg - 91)];
  } else if (i32_angle_deg <= 270) {
    // Read "forward" and inverse value. Note that very first item from
    // table can not be used (since it falls into previous condition)
    return -1 * mi32_sin90[i32_angle_deg - 180];
  } else {
    // Read "backward" and inverse value
    return -1 * mi32_sin90[89 - (i32_angle_deg - 271)];
  }
}

int32_t mf_asin(int32_t i32_sin_x) {
  // Range is "-1" to "+1"
  assert((i32_sin_x >= -1e6) && (i32_sin_x <= 1e6));

  /* Bit theory before proceed to implementation. the sin(x) function return
   * value which is same for 45 degrees and 135 degrees -> can not
   * distinguish what was original value. Technically this function can
   * return -90 ~ +90 degrees.
   */
  if (i32_sin_x >= 0) {
    // Easy one
    for (uint8_t u8_angle = 0;
         u8_angle < (sizeof(mi32_sin90) / sizeof(mi32_sin90[0])); u8_angle++) {
      if (i32_sin_x <= mi32_sin90[u8_angle]) {
        return (int32_t)u8_angle;
      }
    }
    // Should not happen
    assert(0);
  } else {
    // Value is negative - result is same as for positive value, but
    // angle will be negative
    return -1 * mf_asin(-1 * i32_sin_x);
  }

  // Should not happen - ever
  assert(0);
  return INT32_MAX;
}

int32_t mf_cos(int32_t i32_angle_deg) {
  // technically, cosine is just sine shifted by 90 degrees
  return mf_sin(i32_angle_deg + 90);
}

int32_t mf_acos(int32_t i32_cos_x) {
  // Range is "-1" to "+1"
  assert((i32_cos_x >= -1e6) && (i32_cos_x <= 1e6));

  /* Again bit theory. Expected range is 0 ~ 90 for positive numbers and
   * 91 ~ 180 for negative numbers. This is the way how it works on
   * calculator, so why not.
   *
   * The arcsin() is basically same function, but shifted by 90 degrees ->
   * possible to re-use it.
   */
  return (90 - mf_asin(i32_cos_x));
}

int32_t mf_tan(int32_t i32_angle_deg) {
  i32_angle_deg = _get_180_deg_range(i32_angle_deg);

  // Find quadrant
  if (i32_angle_deg <= 90) {
    // Easy one
    return mi32_tan90[i32_angle_deg];
  } else {
    // Reverse value and go "backwards". Regarding to the magic numbers,
    // simply refer to sin_fast() - it is same situation
    return -1 * mi32_tan90[89 - (i32_angle_deg - 91)];
  }
}

int32_t mf_atan(int32_t i32_tan_x) {
  if (i32_tan_x >= 0) {
    // Positive value -> easy one
    for (uint8_t u8_angle = 0;
         u8_angle < (sizeof(mi32_tan90) / sizeof(mi32_tan90[0])); u8_angle++) {
      if (i32_tan_x <= mi32_tan90[u8_angle]) {
        return (int32_t)u8_angle;
      }
    }
    // Should not happen
    assert(0);
  } else {
    // Value is negative - result is same as for positive value, but
    // angle is shifted by 90 degrees, backward (180 - (0~90)) and negative

    // In case that value is lowest possible, but multiplying -1 we would
    // actually get same value (overflow) -> increase by 1
    if (i32_tan_x == INT32_MIN) {
      i32_tan_x++;
    }
    return -1 * mf_atan(-1 * i32_tan_x);
  }

  // Should not happen - ever
  assert(0);
  return INT32_MAX;
}

int32_t mf_get_360_deg_range(int32_t i32_angle_deg) {
  // If already in range, nothing to do
  if ((i32_angle_deg >= 0) && (i32_angle_deg <= 360)) {
    return i32_angle_deg;
  }

  // else need to deal with it
  if (i32_angle_deg > 0) {
    // Angle is positive
    while (i32_angle_deg > 360) {
      i32_angle_deg -= 360;
    }
  } else {
    // Angle is negative
    while (i32_angle_deg < 0) {
      i32_angle_deg += 360;
    }
  }

  return i32_angle_deg;
}

int32_t mf_get_plus_minus_180_deg_range(int32_t i32_angle_deg) {
  // Get range 0~360
  i32_angle_deg = mf_get_360_deg_range(i32_angle_deg);

  // If value is over 180, then angle should be negative -> rotate back by 360
  if (i32_angle_deg > 180) {
    i32_angle_deg -= 360;
  } else {
    // Angle is in range 0~180 -> no need to change
  }

  return i32_angle_deg;
}

uint16_t mf_sqrt_16(uint16_t u16_src) {
  // Easy calculations
  if ((u16_src == 0) || (u16_src == 1)) {
    return u16_src;
  }
  if ((u16_src == 2) || (u16_src == 3)) {
    return 1;
  }

  // Keep eye on number of iterations
  uint8_t u8_hand_brake_cnt = 0;

  uint32_t u32_x = 1;
  uint32_t u32_x_prev = 0;

  while (u32_x != u32_x_prev) {
    u32_x_prev = u32_x;
    u32_x = (u32_x + (u16_src / u32_x)) / 2;

    u8_hand_brake_cnt++;

    // Due to inaccuracy of integer, sometimes result will not be same
    // -> can not converge to results -> typically few iterations is more
    // than accurate -> using this hand brake.
    // During testing was proved that 10 iteration at max is enough to get
    // good accuracy
    if (u8_hand_brake_cnt >= 10) {
      assert(u32_x < UINT16_MAX);
      return (uint16_t)u32_x;
    }
  }

  assert(u32_x < UINT16_MAX);
  return (uint16_t)u32_x;
}

uint32_t mf_sqrt_32(uint32_t u32_src) {
  // Easy calculations
  if ((u32_src == 0) || (u32_src == 1)) {
    return u32_src;
  }
  if ((u32_src == 2) || (u32_src == 3)) {
    return 1;
  }

  // Keep eye on number of iterations
  uint8_t u8_hand_brake_cnt = 0;

  uint64_t u64_x = 1;
  uint64_t u64_x_prev = 0;

  while (u64_x != u64_x_prev) {
    u64_x_prev = u64_x;
    u64_x = (u64_x + (u32_src / u64_x)) / 2;

    u8_hand_brake_cnt++;

    // Due to inaccuracy of integer, sometimes result will not be same
    // -> can not converge to results -> typically few iterations is more
    // than accurate -> using this hand brake.
    // During testing was proved that 13 iteration at max is enough to get
    // good accuracy (you can run tests to verify)
    if (u8_hand_brake_cnt >= 13) {
      assert(u64_x < UINT32_MAX);
      return (uint32_t)u64_x;
    }
  }

  assert(u64_x < UINT32_MAX);
  return (uint32_t)u64_x;
}

uint64_t mf_sqrt_64(uint64_t u64_src) {
  // Easy calculations
  if ((u64_src == 0) || (u64_src == 1)) {
    return u64_src;
  }

  // Keep eye on number of iterations
  uint8_t u8_hand_brake_cnt = 0;

  // Using float instead of double. Using integer would cause significant
  // errors because of missing decimal point
  const float f_Src = (float)u64_src;
  float f_x = f_Src;
  float f_y = 1;

  while (mf_abs_float(f_x - f_y) / mf_abs_float(f_x) > 0.000001) {
    f_x = (f_x + f_y) / 2;
    f_y = f_Src / f_x;

    u8_hand_brake_cnt++;

    // Due to inaccuracy of integer, sometimes result will not be same
    // -> can not converge to results -> typically few iterations is more
    // than accurate -> using this hand brake.
    // During testing was found this "number of iterations" value, which
    // is optimum between processing time and accuracy
    if (u8_hand_brake_cnt == 30) {
      return (uint64_t)f_x;
    }
  }

  return (uint64_t)f_x;
}

int16_t mf_abs_16(int16_t i16_src) {
  if (i16_src < 0) {
    return -1 * i16_src;
  } else {
    return i16_src;
  }
}

int32_t mf_abs_32(int32_t i32_src) {
  if (i32_src < 0) {
    return -1 * i32_src;
  } else {
    return i32_src;
  }
}

int64_t mf_abs_64(int64_t i64_src) {
  if (i64_src < 0) {
    return -1 * i64_src;
  } else {
    return i64_src;
  }
}

float mf_abs_float(float f_src) {
  if (f_src < 0) {
    return -1 * f_src;
  } else {
    return f_src;
  }
}

// ==========================| Internal functions |===========================

static int32_t _get_180_deg_range(int32_t i32_angle) {
  // Make it fast
  i32_angle = mf_get_360_deg_range(i32_angle);

  if ((i32_angle >= 0) && (i32_angle <= 180)) {
    // Already in range, nothing to do
  } else {
    // Range is 181 ~ 360 -> reduce by 180
    i32_angle -= 180;
  }

  return i32_angle;
}
