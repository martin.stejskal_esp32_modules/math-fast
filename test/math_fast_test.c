#include "../math_fast.h"

#include <assert.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

/**
 * @brief Range of tested angles
 *
 * @{
 */
#define ANGLE_MIN (-3600)
#define ANGLE_MAX (-1 * ANGLE_MIN)
/**
 * @}
 */

/**
 * @brief Range of tested X, Y values
 *
 * @{
 */
#define MIN_XY (-66000)
#define MAX_XY (-1 * MIN_XY)
/**
 * @}
 */

#define SQRT_QUICK_TEST_MAX (10000000UL)
#define SQRT_SLOW_64_BIT_TEST_MAX (54330320 * SQRT_QUICK_TEST_MAX)

/**
 * @brief Relative tolerance in % when calculating square root
 */
#define SQRT_REL_TOL (1)

///@brief Maximum error tolerance in ppm
#define DIFF_TOLERANCE (1)

/**
 * @brief Tells how many errors have to be detected to abort current test
 *
 * This avoid spaming to output in case something went wrong
 */
#define GIVE_UP_AFTER_N_ERRORS (10)

typedef struct {
  bool b_expected_result;
  int32_t i32_angle_deg;
  int32_t i32_min_deg;
  int32_t i32_max_deg;
} ts_angle_in_range;

int32_t test_sin(void) {
  int32_t i32_num_of_problems = 0;
  int32_t i32_tests = 0;
  int32_t i32_sin;
  double f_sin;
  int32_t i32_real_sin;
  int32_t i32_sin_diff;

  for (int32_t i32_angle = ANGLE_MIN; i32_angle <= ANGLE_MAX; i32_angle++) {
    i32_sin = mf_sin(i32_angle);
    f_sin = sin(M_PI * i32_angle / 180) * 1e6;
    i32_real_sin = (int32_t)f_sin;
    i32_sin_diff = i32_real_sin - i32_sin;
    // Get absolute value of difference
    if (i32_sin_diff < 0) {
      i32_sin_diff *= -1;
    }

    if (i32_sin_diff > DIFF_TOLERANCE) {
      i32_num_of_problems++;
      printf("Angle: %d | sin(x): %d | table: %d | Diff: %d\n", i32_angle,
             i32_real_sin, i32_sin, i32_sin_diff);
    }

    i32_tests++;
    if (i32_num_of_problems > GIVE_UP_AFTER_N_ERRORS) {
      return i32_num_of_problems;
    }
  }
  printf("Sin test done. Problems: %d (tests: %d)\n\n", i32_num_of_problems,
         i32_tests);

  return i32_num_of_problems;
}

int32_t test_cos(void) {
  int32_t i32_num_of_problems = 0;
  int32_t i32_tests = 0;
  int32_t i32_cos;
  double f_cos;
  int32_t i32_real_cos;
  int32_t i32_cos_diff;

  for (int32_t i32_angle = ANGLE_MIN; i32_angle <= ANGLE_MAX; i32_angle++) {
    i32_cos = mf_cos(i32_angle);
    f_cos = cos(M_PI * i32_angle / 180) * 1e6;
    i32_real_cos = (int32_t)f_cos;
    i32_cos_diff = i32_real_cos - i32_cos;

    // Get absolute value of difference
    if (i32_cos_diff < 0) {
      i32_cos_diff *= -1;
    }

    if (i32_cos_diff > DIFF_TOLERANCE) {
      i32_num_of_problems++;
      printf("Angle: %d | cos(x): %d | table: %d | Diff: %d\n", i32_angle,
             i32_real_cos, i32_cos, i32_cos_diff);
    }

    i32_tests++;
    if (i32_num_of_problems > GIVE_UP_AFTER_N_ERRORS) {
      return i32_num_of_problems;
    }
  }
  printf("Cos test done. Problems: %d (tests: %d)\n\n", i32_num_of_problems,
         i32_tests);

  return i32_num_of_problems;
}

int32_t test_tan(void) {
  int32_t i32_num_of_problems = 0;
  int32_t i32_tests = 0;
  int32_t i32_tan;
  double f_tan;
  int32_t i32_real_tan;
  int32_t i32_tan_diff;

  for (int32_t i32_angle = ANGLE_MIN; i32_angle <= ANGLE_MAX; i32_angle++) {
    i32_tan = mf_tan(i32_angle);
    f_tan = tan(M_PI * i32_angle / 180) * 1e6;
    i32_real_tan = (int32_t)f_tan;
    i32_tan_diff = i32_real_tan - i32_tan;
    // Get absolute value of difference
    if (i32_tan_diff < 0) {
      i32_tan_diff *= -1;
    }

    if (i32_tan_diff > DIFF_TOLERANCE) {
      i32_num_of_problems++;
      printf("Angle: %d | tan(x): %d | table: %d | Diff: %d\n", i32_angle,
             i32_real_tan, i32_tan, i32_tan_diff);
    }

    i32_tests++;
    if (i32_num_of_problems > GIVE_UP_AFTER_N_ERRORS) {
      return i32_num_of_problems;
    }
  }
  printf("Tan test done. Problems: %d (tests: %d)\n\n", i32_num_of_problems,
         i32_tests);

  return i32_num_of_problems;
}

int32_t test_asin(void) {
  int32_t i32_num_of_problems = 0;
  int32_t i32_tests = 0;

  int32_t i32_angle;
  double f_angle;
  int32_t i32_real_angle;
  int32_t i32_angleDiff;

  for (int32_t i32sinX = -1e6; i32sinX <= 1e6; i32sinX++) {
    i32_angle = mf_asin(i32sinX);
    f_angle = asin(((double)i32sinX) / 1e6) * (180 / M_PI);

    i32_real_angle = (int32_t)f_angle;
    i32_angleDiff = i32_real_angle - i32_angle;

    // Get absolute difference
    if (i32_angleDiff < 0) {
      i32_angleDiff *= -1;
    }

    if (i32_angleDiff > DIFF_TOLERANCE) {
      i32_num_of_problems++;
      printf("Value: %d | asin(x): %d | table: %d | Diff: %d\n", i32sinX,
             i32_real_angle, i32_angle, i32_angleDiff);
    }
    i32_tests++;
    if (i32_num_of_problems > GIVE_UP_AFTER_N_ERRORS) {
      return i32_num_of_problems;
    }
  }
  printf("Asin test done. Problems: %d (tests: %d)\n\n", i32_num_of_problems,
         i32_tests);

  return i32_num_of_problems;
}

int32_t test_acos(void) {
  int32_t i32_num_of_problems = 0;
  int32_t i32_tests = 0;

  int32_t i32_angle;
  double f_angle;
  int32_t i32_real_angle;
  int32_t i32_angleDiff;

  for (int32_t i32_cosX = -1e6; i32_cosX <= 1e6; i32_cosX++) {
    i32_angle = mf_acos(i32_cosX);
    f_angle = acos(((double)i32_cosX) / 1e6) * (180 / M_PI);

    i32_real_angle = (int32_t)f_angle;
    i32_angleDiff = i32_real_angle - i32_angle;

    // Get absolute difference
    if (i32_angleDiff < 0) {
      i32_angleDiff *= -1;
    }

    if (i32_angleDiff > DIFF_TOLERANCE) {
      i32_num_of_problems++;
      printf("Value: %d | acos(x): %d | table: %d | Diff: %d\n", i32_cosX,
             i32_real_angle, i32_angle, i32_angleDiff);
    }
    i32_tests++;
    if (i32_num_of_problems > GIVE_UP_AFTER_N_ERRORS) {
      return i32_num_of_problems;
    }
  }
  printf("Acos test done. Problems: %d (tests: %d)\n\n", i32_num_of_problems,
         i32_tests);

  return i32_num_of_problems;
}

int32_t test_atan(void) {
  int32_t i32_num_of_problems = 0;
  int32_t i32_tests = 0;

  int32_t i32_angle;
  double f_angle;
  int32_t i32_real_angle;
  int32_t i32_angleDiff;

  printf("Atan test will take a while...\n");
  // Tangent is more tricky. Also range of values is quite higher, since
  // it is from "minus infinity" up to "plus infinity" -> bit reduce it
  for (int32_t i32_cosX = -60e6; i32_cosX <= 60e6; i32_cosX++) {
    i32_angle = mf_atan(i32_cosX);
    f_angle = atan(((double)i32_cosX) / 1e6) * (180 / M_PI);

    i32_real_angle = (int32_t)f_angle;
    i32_angleDiff = i32_real_angle - i32_angle;

    // Get absolute difference
    if (i32_angleDiff < 0) {
      i32_angleDiff *= -1;
    }

    if (i32_angleDiff > DIFF_TOLERANCE) {
      i32_num_of_problems++;
      printf("Value: %d | atan(x): %d | table: %d | Diff: %d\n", i32_cosX,
             i32_real_angle, i32_angle, i32_angleDiff);
    }
    i32_tests++;
    if (i32_num_of_problems > GIVE_UP_AFTER_N_ERRORS) {
      return i32_num_of_problems;
    }
  }
  printf("Atan test done. Problems: %d (tests: %d)\n\n", i32_num_of_problems,
         i32_tests);

  return i32_num_of_problems;
}

int32_t test_get_angle_quick(void) {
  int32_t i32_num_of_problems = 0;
  int32_t i32_angle;

  i32_angle = mf_get_angle_from_xy(0, 0);
  if (i32_angle != 0) {
    i32_num_of_problems++;
    printf("getAngle: got: %d | expected: %d\n", i32_angle, 0);
  }

  i32_angle = mf_get_angle_from_xy(0, 10);
  if (i32_angle != 90) {
    i32_num_of_problems++;
    printf("getAngle: got: %d | expected: %d\n", i32_angle, 90);
  }

  i32_angle = mf_get_angle_from_xy(0, -10);
  if (i32_angle != 270) {
    i32_num_of_problems++;
    printf("getAngle: got: %d | expected: %d\n", i32_angle, 270);
  }

  i32_angle = mf_get_angle_from_xy(10, 10);
  if (i32_angle != 45) {
    i32_num_of_problems++;
    printf("getAngle: got: %d | expected: %d\n", i32_angle, 45);
  }

  i32_angle = mf_get_angle_from_xy(-10, 10);
  if (i32_angle != 135) {
    i32_num_of_problems++;
    printf("getAngle: got: %d | expected: %d\n", i32_angle, 135);
  }

  i32_angle = mf_get_angle_from_xy(-10, -10);
  if (i32_angle != 225) {
    i32_num_of_problems++;
    printf("getAngle: got: %d | expected: %d\n", i32_angle, 225);
  }

  i32_angle = mf_get_angle_from_xy(10, -10);
  if (i32_angle != 315) {
    i32_num_of_problems++;
    printf("getAngle: got: %d | expected: %d\n", i32_angle, 315);
  }

  printf("Get angle test done: Problems: %d\n\n", i32_num_of_problems);

  return i32_num_of_problems;
}

int32_t test_get_relative_angle_quick(void) {
  int32_t i32_num_of_problems = 0;
  int32_t i32_diff_angle = 0;

  i32_diff_angle = mf_relative_angle_difference(0, 90);
  if (i32_diff_angle != 90) {
    i32_num_of_problems++;
  }

  i32_diff_angle = mf_relative_angle_difference(135, 325);
  if (i32_diff_angle != -170) {
    i32_num_of_problems++;
  }

  i32_diff_angle = mf_relative_angle_difference(0, 180);
  if (i32_diff_angle != 180) {
    i32_num_of_problems++;
  }

  i32_diff_angle = mf_relative_angle_difference(530, 100);
  if (i32_diff_angle != -70) {
    i32_num_of_problems++;
  }

  i32_diff_angle = mf_relative_angle_difference(0, 360);
  if (i32_diff_angle != 0) {
    i32_num_of_problems++;
  }

  printf("Get relative angle quick done: Problems: %d\n\n",
         i32_num_of_problems);

  return i32_num_of_problems;
}

int32_t test_get_angle_full(void) {
  int32_t i32_num_of_problems = 0;
  int32_t i32_angle, i32_real_angle, i32_angleDiff;
  double f_angle;

  int64_t i64_case_cnt = 0;
  const int64_t i64_tot_cases =
      (int64_t)(MAX_XY - MIN_XY) * (int64_t)(MAX_XY - MIN_XY);
  const int64_t i64_progress_step = 10000000;
  int64_t i64_next_report = 0;

  printf("Testing getAngle(). This gonna be loooong ride\n");
  // MIN_XY
  for (int32_t i32x = MIN_XY; i32x < MAX_XY; i32x++) {
    for (int32_t i32y = MIN_XY; i32y < MAX_XY; i32y++) {
      // printf("x: %d y: %d\n", i32x, i32y);
      i32_angle = mf_get_angle_from_xy(i32x, i32y);
      f_angle = atan((double)i32y / (double)i32x) * (180 / M_PI);
      i32_real_angle = (int32_t)f_angle;
      i32_angleDiff = i32_real_angle - i32_angle;

      if (i32_angleDiff > DIFF_TOLERANCE) {
        i32_num_of_problems++;
        printf(
            "Value: x: %d y: %d | atan2(y/x): %d | table: %d |"
            " Diff: %d\n",
            i32x, i32y, i32_real_angle, i32_angle, i32_angleDiff);
        if (i32_num_of_problems > GIVE_UP_AFTER_N_ERRORS) {
          return i32_num_of_problems;
        }
      }

      i64_case_cnt++;

      if (i64_case_cnt >= i64_next_report) {
        printf("%ld M of %ld M\n", i64_case_cnt / 1000000,
               i64_tot_cases / 1000000);
        i64_next_report += i64_progress_step;
      }
    }
  }

  printf("Get angle test (full) done: Problems: %d\n\n", i32_num_of_problems);

  return i32_num_of_problems;
}

int32_t get_xy_coordinates_test_small(const int32_t i32_init_x,
                                      const int32_t i32_init_y,
                                      const int32_t i32_length) {
  assert(i32_length >= 0);

  int32_t i32_num_of_problems = 0;
  int32_t i32_res_x, i32_res_y;

  int32_t i32_diff_x, i32_diff_y;

  int32_t i32_real_x, i32_real_y;
  double f_real_x, f_real_y;

  for (int32_t i32_angle = ANGLE_MIN; i32_angle <= ANGLE_MAX; i32_angle++) {
    mf_get_xy_coordinates(i32_init_x, i32_init_y, i32_length, i32_angle,
                          &i32_res_x, &i32_res_y);
    f_real_x = cos(M_PI * i32_angle / 180) * i32_length;
    f_real_y = sin(M_PI * i32_angle / 180) * i32_length;

    i32_real_x = (int32_t)f_real_x + i32_init_x;
    i32_real_y = (int32_t)f_real_y + i32_init_y;

    // Calculate difference as absolute value
    i32_diff_x = i32_real_x - i32_res_x;
    if (i32_diff_x < 0) {
      i32_diff_x *= -1;
    }

    i32_diff_y = i32_real_y - i32_res_y;
    if (i32_diff_y < 0) {
      i32_diff_y *= -1;
    }

    // Just for debug purpose
    // printf("Get XY: angle: %d | x: %d | y: %d\n", i32_angle, i32resX,
    //         i32resY);

    if (i32_diff_x > DIFF_TOLERANCE) {
      i32_num_of_problems++;
      printf(
          "Get XY: Angle: %d | x: %d (%d) | Diff: %d |"
          "Init x: %d y: %d\n",
          i32_angle, i32_real_x, i32_res_x, i32_diff_x, i32_init_x, i32_init_y);
    }
    if (i32_diff_y > DIFF_TOLERANCE) {
      i32_num_of_problems++;
      printf(
          "Get XY: Angle: %d | y: %d (%d) | Diff: %d |"
          "Init x: %d y: %d\n",
          i32_angle, i32_real_y, i32_res_y, i32_diff_y, i32_init_x, i32_init_y);
    }

    if (i32_num_of_problems > GIVE_UP_AFTER_N_ERRORS) {
      return i32_num_of_problems;
    }
  }

  return i32_num_of_problems;
}

int32_t test_get_xy_coordinates_full(void) {
  int32_t i32_num_of_problems = 0;
  const int32_t i32_tot_num_of_cases = (100 - (-100)) * (100 - (-100)) * (100);
  int32_t i32_test_cnt = 0;
  int32_t i32_show_status_cnt = 0;

  printf("Testing getXYcoordinates(). This will take a while...\n");
  for (int32_t i32_init_x = -100; i32_init_x <= 100; i32_init_x++) {
    for (int32_t i32_init_y = -100; i32_init_y <= 100; i32_init_y++) {
      for (int32_t i32_length = 0; i32_length <= 100; i32_length++) {
        i32_num_of_problems +=
            get_xy_coordinates_test_small(i32_init_x, i32_init_y, i32_length);

        if (i32_num_of_problems > GIVE_UP_AFTER_N_ERRORS) {
          return i32_num_of_problems;
        }

        if (i32_test_cnt >= i32_show_status_cnt) {
          i32_show_status_cnt += 1000;
          printf("%d of %d (%.3f %%)\n", i32_test_cnt, i32_tot_num_of_cases,
                 (float)i32_test_cnt * 100 / (float)i32_tot_num_of_cases);
        }

        i32_test_cnt++;
      }
    }
  }

  return i32_num_of_problems;
}

int32_t test_is_angle_in_range_fast(void) {
  int32_t i32_num_of_problems = 0;

  // List of test cases
  const ts_angle_in_range as_tests[] = {
      {
          .b_expected_result = 1,
          .i32_angle_deg = 35,
          .i32_min_deg = 25,
          .i32_max_deg = 45,
      },
      {
          .b_expected_result = 0,
          .i32_angle_deg = 50,
          .i32_min_deg = 67,
          .i32_max_deg = 45,
      },
      {
          .b_expected_result = 0,
          .i32_angle_deg = 86,
          .i32_min_deg = 87,
          .i32_max_deg = 89,
      },
      {
          .b_expected_result = 1,
          .i32_angle_deg = 186,
          .i32_min_deg = 89,
          .i32_max_deg = 87,
      },
      {
          .b_expected_result = 1,
          .i32_angle_deg = -132,
          .i32_min_deg = 50,
          .i32_max_deg = -30,
      },
      {
          .b_expected_result = 0,
          .i32_angle_deg = -13,
          .i32_min_deg = 50,
          .i32_max_deg = -30,
      },
      {
          .b_expected_result = 0,
          .i32_angle_deg = 0,
          .i32_min_deg = 10,
          .i32_max_deg = -10,
      },
      {
          .b_expected_result = 1,
          .i32_angle_deg = 0,
          .i32_min_deg = -10,
          .i32_max_deg = 10,
      },
      {
          .b_expected_result = 0,
          .i32_angle_deg = 185,
          .i32_min_deg = -170,
          .i32_max_deg = 160,
      },
      {
          .b_expected_result = 1,
          .i32_angle_deg = 33,
          .i32_min_deg = 33,
          .i32_max_deg = 50,
      },
      {
          .b_expected_result = 1,
          .i32_angle_deg = 64,
          .i32_min_deg = 30,
          .i32_max_deg = 64,
      },
      {
          .b_expected_result = 1,
          .i32_angle_deg = 185,
          .i32_min_deg = -175,
          .i32_max_deg = 160,
      },
      {
          .b_expected_result = 1,
          .i32_angle_deg = 0,
          .i32_min_deg = 0,
          .i32_max_deg = 16,
      },
      {
          .b_expected_result = 1,
          .i32_angle_deg = 0,
          .i32_min_deg = -175,
          .i32_max_deg = 0,
      },

      {
          .b_expected_result = 0,
          .i32_angle_deg = 1,
          .i32_min_deg = -175,
          .i32_max_deg = 0,
      },
      {
          .b_expected_result = 0,
          .i32_angle_deg = -1,
          .i32_min_deg = 0,
          .i32_max_deg = 180,
      },
  };

  bool b_result;
  const ts_angle_in_range *p_test;

  for (int i_test_idx = 0;
       i_test_idx < (sizeof(as_tests) / sizeof(as_tests[0])); i_test_idx++) {
    p_test = &(as_tests[i_test_idx]);

    b_result = mf_is_angle_in_range(p_test->i32_angle_deg, p_test->i32_min_deg,
                                    p_test->i32_max_deg);

    if (b_result != p_test->b_expected_result) {
      printf(
          "Is angle in range error: test angle: %d min: %d max: %d ; result: "
          "%d\n",
          p_test->i32_angle_deg, p_test->i32_min_deg, p_test->i32_max_deg,
          b_result);
      i32_num_of_problems++;
      if (i32_num_of_problems > GIVE_UP_AFTER_N_ERRORS) {
        break;
      }
    }
  }

  return i32_num_of_problems;
}

int32_t test_sqrt_16_fast_valid_range(void) {
  int32_t i32_num_of_problems = 0, i32_rel_diff_perc, i32_abs_diff;

  double d_real_res;
  uint16_t u16_real_res, u16_calc_ces;

  for (uint32_t u32_num = 0; u32_num < UINT16_MAX; u32_num++) {
    d_real_res = sqrt((double)u32_num);

    u16_real_res = (uint16_t)d_real_res;
    u16_calc_ces = mf_sqrt_16(u32_num);

    // Calculate relative error
    if (u16_real_res != 0) {
      // If non zero
      i32_rel_diff_perc = 100 - (100 * u16_calc_ces) / u16_real_res;
      if (i32_rel_diff_perc < 0) {
        i32_rel_diff_perc *= -1;
      }
    } else {
      // Result is zero - have to match
      if (u16_real_res != u16_calc_ces) {
        i32_rel_diff_perc = 100;
      } else {
        i32_rel_diff_perc = 0;
      }
    }
    // Calculate absolute error
    i32_abs_diff = u16_real_res - u16_calc_ces;
    if (i32_abs_diff < 0) {
      i32_abs_diff *= -1;
    }

    if ((i32_rel_diff_perc > SQRT_REL_TOL) && (i32_abs_diff > DIFF_TOLERANCE)) {
      i32_num_of_problems++;
      printf("sqrt(%d): Real: %d | Calculated: %d | Rel Diff: %d%%\n", u32_num,
             u16_real_res, u16_calc_ces, i32_rel_diff_perc);
    }

    if (i32_num_of_problems > GIVE_UP_AFTER_N_ERRORS) {
      return i32_num_of_problems;
    }
  }

  return i32_num_of_problems;
}

int32_t test_sqrt_32_fast_valid_range_quickly(void) {
  int32_t i32_num_of_problems = 0, i32_rel_diff_perc, i32_abs_diff;

  double d_real_res;
  uint32_t u32_real_res, u32_calc_res;

  for (uint64_t u64_num = 0; u64_num < SQRT_QUICK_TEST_MAX; u64_num++) {
    d_real_res = sqrt((double)u64_num);

    u32_real_res = (uint32_t)d_real_res;
    u32_calc_res = mf_sqrt_32(u64_num);

    // Calculate relative error
    if (u32_real_res != 0) {
      // If non zero
      i32_rel_diff_perc = 100 - (100 * u32_calc_res) / u32_real_res;
      if (i32_rel_diff_perc < 0) {
        i32_rel_diff_perc *= -1;
      }
    } else {
      // Result is zero - have to match
      if (u32_real_res != u32_calc_res) {
        i32_rel_diff_perc = 100;
      } else {
        i32_rel_diff_perc = 0;
      }
    }
    // Calculate absolute error
    i32_abs_diff = u32_real_res - u32_calc_res;
    if (i32_abs_diff < 0) {
      i32_abs_diff *= -1;
    }

    if ((i32_rel_diff_perc > SQRT_REL_TOL) && (i32_abs_diff > DIFF_TOLERANCE)) {
      i32_num_of_problems++;
      printf("sqrt(%ld): Real: %d | Calculated: %d | Rel Diff: %d%%\n", u64_num,
             u32_real_res, u32_calc_res, i32_rel_diff_perc);
    }

    if (i32_num_of_problems > GIVE_UP_AFTER_N_ERRORS) {
      return i32_num_of_problems;
    }
  }

  return i32_num_of_problems;
}

int32_t test_sqrt_64_fast_valid_range_quickly(void) {
  int32_t i32_num_of_problems = 0, i32_rel_diff_perc, i32_abs_diff;

  double d_real_res;
  uint64_t u64_real_res, u64_calc_res;

  for (uint64_t u64_num = 0; u64_num < SQRT_QUICK_TEST_MAX; u64_num++) {
    d_real_res = sqrt((double)u64_num);

    u64_real_res = (uint64_t)d_real_res;
    u64_calc_res = mf_sqrt_64(u64_num);

    // Calculate relative error
    if (u64_real_res != 0) {
      // If non zero
      i32_rel_diff_perc = 100 - (100 * u64_calc_res) / u64_real_res;
      if (i32_rel_diff_perc < 0) {
        i32_rel_diff_perc *= -1;
      }
    } else {
      // Result is zero - have to match
      if (u64_real_res != u64_calc_res) {
        i32_rel_diff_perc = 100;
      } else {
        i32_rel_diff_perc = 0;
      }
    }
    // Calculate absolute error
    i32_abs_diff = u64_real_res - u64_calc_res;
    if (i32_abs_diff < 0) {
      i32_abs_diff *= -1;
    }

    if ((i32_rel_diff_perc > SQRT_REL_TOL) && (i32_abs_diff > DIFF_TOLERANCE)) {
      i32_num_of_problems++;
      printf("sqrt(%ld): Real: %ld | Calculated: %ld | Rel Diff: %d%%\n",
             u64_num, u64_real_res, u64_calc_res, i32_rel_diff_perc);
    }

    if (i32_num_of_problems > GIVE_UP_AFTER_N_ERRORS) {
      return i32_num_of_problems;
    }
  }

  return i32_num_of_problems;
}

int32_t test_sqrt_64_fast_range_roughly(void) {
  int32_t i32_num_of_problems = 0, i32_rel_dif, i32_abs_diff;

  double d_real_res;
  uint64_t u64_real_res, u64_calc_res;

  const int64_t i64_progress_step = 100000000000;
  int64_t i64_next_report = 0;

  printf("Testing square root function. This gonna be loooong ride\n");

  for (uint64_t u64_num = 0; u64_num < SQRT_SLOW_64_BIT_TEST_MAX; u64_num++) {
    d_real_res = sqrt((double)u64_num);

    u64_real_res = (uint64_t)d_real_res;
    u64_calc_res = mf_sqrt_64(u64_num);

    // Calculate relative error
    if (u64_real_res != 0) {
      // If non zero
      i32_rel_dif = 100 - (100 * u64_calc_res) / u64_real_res;
      if (i32_rel_dif < 0) {
        i32_rel_dif *= -1;
      }
    } else {
      // Result is zero - have to match
      if (u64_real_res != u64_calc_res) {
        i32_rel_dif = 100;
      } else {
        i32_rel_dif = 0;
      }
    }
    // Calculate absolute error
    i32_abs_diff = u64_real_res - u64_calc_res;
    if (i32_abs_diff < 0) {
      i32_abs_diff *= -1;
    }

    if ((i32_rel_dif > SQRT_REL_TOL) && (i32_abs_diff > DIFF_TOLERANCE)) {
      i32_num_of_problems++;
      printf("sqrt(%ld): Real: %ld | Calculated: %ld | Rel Diff: %d\n", u64_num,
             u64_real_res, u64_calc_res, i32_rel_dif);
    }

    if (i32_num_of_problems > GIVE_UP_AFTER_N_ERRORS) {
      return i32_num_of_problems;
    }

    if (u64_num >= i64_next_report) {
      printf("%ld M of %ld M\n", u64_num / 1000000,
             SQRT_SLOW_64_BIT_TEST_MAX / 1000000);
      i64_next_report += i64_progress_step;
    }

    // To speed up a bit not every single value is tested. Let's say that
    // next value is k*x previous number
    if (u64_num > 1000000) {
      u64_num += (u64_num / 1000000);
    }
  }

  return i32_num_of_problems;
}

int main(int argc, char **argv) {
  bool b_full_test = true;

  // If any argument given, check if it is "--fast" option
  if (argc > 1) {
    if (strcmp(argv[1], "--fast") == 0) {
      b_full_test = false;
      printf("Performing FAST test\n\n");
    } else {
      printf("Unknown option %s\n\n", argv[1]);
      return -1;
    }
  } else {
    printf(
        "Performing full test. For fast option, add \"--fast\" "
        "argument\n\n");
  }

  int32_t i32_num_of_problems = 0;

  i32_num_of_problems += test_get_relative_angle_quick();
  i32_num_of_problems += test_sin();
  i32_num_of_problems += test_cos();
  i32_num_of_problems += test_asin();
  i32_num_of_problems += test_acos();
  i32_num_of_problems += test_tan();
  i32_num_of_problems += test_atan();

  i32_num_of_problems += test_get_angle_quick();

  if (b_full_test) {
    i32_num_of_problems += test_get_angle_full();
    i32_num_of_problems += test_get_xy_coordinates_full();
  }

  i32_num_of_problems += test_is_angle_in_range_fast();

  i32_num_of_problems += test_sqrt_16_fast_valid_range();
  i32_num_of_problems += test_sqrt_32_fast_valid_range_quickly();
  i32_num_of_problems += test_sqrt_64_fast_valid_range_quickly();

  if (b_full_test) {
    i32_num_of_problems += test_sqrt_64_fast_range_roughly();
  }

  printf("\n\nTotal number of problems: %d\n", i32_num_of_problems);

  return (int)i32_num_of_problems;
}
