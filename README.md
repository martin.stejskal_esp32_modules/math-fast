# About

* Set of simplified mathematical functions, which focus on performance over
  accuracy
* Typical usage is on embedded platforms, where accuracy is not as important as
  performance
* For list of available functions, please refer to [header file](math_fast.h)

